﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeDTO 
    {
        /// <summary>
        /// Guid сотрудника.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Имя.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия.
        /// </summary>
        public string LastName { get; set; }
        
        /// <summary>
        /// Email.
        /// </summary>
        public string Email { get; set; }
        
        /// <summary>
        /// Роли : Admin, PartnerManager
        /// </summary>
        public string Role { get; set; }

        /// <summary>
        /// Количество доступных промокодов.
        /// </summary>
        public int AppliedPromocodesCount { get; set; }

    }
}
