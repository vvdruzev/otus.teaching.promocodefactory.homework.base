﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);

        /// <summary>
        /// Добавление сотруудника.
        /// </summary>
        /// <param name="item"> </param>
        /// <returns></returns>
        Task AddAsync(T item);

        /// <summary>
        /// Обновление информации о сотруднике
        /// </summary>
        /// <param name="itemNew"></param>
        Task UpdateAsync(T itemNew);

        /// <summary>
        /// Удаление.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<bool> DeleteAsync(T item);

    }
}