﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data.ToList();
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        /// <summary>
        /// Добавление элемента.
        /// </summary>
        /// <returns></returns>
        public Task AddAsync(T item)
        {
            Data.Add(item);
            return Task.CompletedTask;
        }

        /// <summary>
        /// Обновление информации элемента
        /// </summary>
        /// <param name="itemNew"></param>
        /// <returns></returns>
        public Task UpdateAsync(T itemNew)
        {
            var itemOld = Data.FirstOrDefault(x => x.Id == itemNew.Id);
            Data.Remove(itemOld);
            Data.Add(itemNew);
            return Task.CompletedTask;
        }

        /// <summary>
        /// Удаление элемента
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<bool> DeleteAsync(T item)
        {
            return Task.FromResult(Data.Remove(item));
        }
    }
}